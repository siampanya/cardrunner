const express = require('express');
const rounter = express.Router();
const url = 'localhost:27017/cardrunner';
const db = require('monk')(url);
// var nodemailer = require('nodemailer');

var server = require('http').Server(express);
const io = require('socket.io')(server);
server.listen(8088);

let api_key = "dee5be543230451619bfb578eed7e76a-ed4dc7c4-8974dfa0";


const mailgun = require("mailgun-js");
const DOMAIN = 'cardrunner.co';
const mg = mailgun({ apiKey: api_key, domain: DOMAIN });

// var transporter = nodemailer.createTransport({
//     host: 'smtp.gmail.com',
//     port: 587,
//     secure: false,
//     auth: {
//         user: 'crnservice2020@gmail.com',
//         pass: '234567890Dtac'
//     }
// });

var dateS;
setInterval(() => {
    dateS = new Date();
}, 1000)


function adateSettign(add) {
    var d = new Date();
    d.setDate(d.getDate() + add)
    return d

}

function atimeSettign(add) {
    var d = new Date();
    d.setHours(d.getHours() + add)
    return d
}

function datecheckingCliam(dat) {
    var d = new Date();
    dat = new Date(dat)
    var Stime = dat.getTime() / 1000;
    var nowTime = d.getTime() / 1000;

    return nowTime - Stime

}

var members = db.get("members");
var ticket = db.get("ticket");
var withdraw = db.get("withdraw");
var deposit = db.get("deposit");
var allwithdraw = db.get("allwithdraw");
var serverIP = db.get("serverIP");
var post = db.get("post");
rounter.route('/members/usersalls').get((req, res) => {
    members.find({}, { fields: { fullname: 1, email: 1, Createis: 1 }, sort: { Createis: -1 }, limit: 5 }).then(doc => {
        return res.status(200).json({
            status: 200,
            data: doc,
            date: dateS
        })
    })
})

rounter.route('/members/withdrawenders').get((req, res) => {
    withdraw.aggregate([{
            $match: {
                type: 1
            }
        },
        {
            $group: {
                _id: '',
                totals: { $sum: "$amount" },
            }
        }
    ]).then(doc => {
        return res.status(200).json({
            status: 200,
            data: doc,
            date: dateS
        })
    })
})

rounter.route('/members/count').get((req, res) => {
    members.count({}).then(doc => {
        return res.status(200).json({
            status: 200,
            count: doc,
            date: dateS
        })
    });
})

rounter.route('/members/registers').post((req, res) => {
    var reffer;
    if (req.body.ref) {
        reffer = req.body.ref
    } else {
        reffer = null
    }

    serverIP.count({ ip: req.body.ip }).then(doc => {
        if (doc > 5) {
            return res.status(200).json({
                status: 404,
                msg: 'บล๊อกการเข้าถึงชั่วคราว',
                date: dateS
            })
        } else {
            registers()
        }

    })

    function registers() {
        members.count({ email: req.body.email }).then(doc => {
            if (doc > 0) {
                return res.status(200).json({
                    status: 404,
                    msg: 'มีการใช้งานแล้ว',
                    date: dateS
                })
            } else {
                registerCoolong()
            }
        })
    }

    function registerCoolong() {
        members.insert({
            fullname: req.body.fullname,
            email: req.body.email,
            pwd: req.body.pwd,
            wallet: 0,
            ref: reffer,
            Createis: dateS
        }, { fields: { _id: 1 } }).then((doc) => {
            io.emit('usersDetails', {
                fullname: req.body.fullname,
                email: req.body.email,
                Createis: dateS
            });
            io.emit('counterAsUSER', { count: 1 })
            addTicketToID(doc._id.toString())
            return res.status(200).json({
                status: 200,
                msg: 'ลงทะเบียนสำเร็จ',
                date: dateS
            })
        });
    }

    function addTicketToID(a) {
        serverIP.insert({
                ip: req.body.ip,
                email: req.body.email
            })
            // ticket.insert({
            //     memID: a,
            //     pkname: "Beginner Free",
            //     package: 1,
            //     persec: 0.00009722222222,
            //     limit: 28800,
            //     running: 28800,
            //     dateBuy: dateS,
            //     dateExpire: adateSettign(60)
            // }).then(() => {
            //     serverIP.insert({
            //         ip: req.body.ip,
            //         email: req.body.email
            //     })
            // })
    }


});

rounter.route('/members/genreateToketn').post((req, res) => {
    members.count({ email: req.body.email, pwd: req.body.pwd }).then(doc => {
        if (doc === 1) {
            PassToken()
        } else {
            return res.status(200).json({
                status: 484,
                msg: 'ไม่พบผู้ใช้งานโปรดลองอีกครั้ง !',
                date: dateS
            })
        }
    });

    function PassToken() {
        members.findOne({ email: req.body.email }, { fields: { _id: 1, fullname: 1, email: 1, wallet: 1, point: 1, cash: 1 } }).then(doc => {
            return res.status(200).json({
                status: 200,
                msg: 'เข้าสู่ระบบสำเร็จ',
                data: doc,
                date: dateS
            })
        })
    }
});




rounter.route('/members/:id/buyTicket').post((req, res) => {

    function packed(a) {

        if (a === 1) {

            return 300
        } else if (a === 2) {

            return 550
        } else if (a === 3) {

            return 1250
        } else if (a === 4) {

            return 3500
        } else if (a === 5) {

            return 7000
        } else if (a === 6) {

            return 15000
        } else if (a === 7) {

            return 30000
        } else if (a === 8) {

            return 50000
        }
    }

    function calculations(a) {

        if (a === 1) {
            return { name: 'Beginner', payment: 0.00009722222222, limit: 28800, start: 28800, price: 300 }

        } else if (a === 2) {
            return { name: 'Member', payment: 0.0001909722222, limit: 28800, start: 28800, price: 550 }

        } else if (a === 3) {
            return { name: 'Standard', payment: 0.000462962963, limit: 28800, start: 28800, price: 1250 }

        } else if (a === 4) {
            return { name: 'Silver', payment: 0.001397569444, limit: 28800, start: 28800, price: 3500 }

        } else if (a === 5) {
            return { name: 'Gold', payment: 0.002916666667, limit: 28800, start: 28800, price: 7000 }

        } else if (a === 6) {
            return { name: 'Platinum', payment: 0.006597222222, limit: 28800, start: 28800, price: 15000 }
        } else if (a === 7) {

            return { name: 'Exclusive', payment: 0.01354166667, limit: 28800, start: 28800, price: 30000 }
        } else if (a === 8) {
            return { name: 'Premium', payment: 0.0234375, limit: 28800, start: 28800, price: 50000 }
        }

    }

    function promotions_freeonebuyone(s, upline) {
        if (s === 5) {
            let be1 = { name: 'Member Promotion', payment: 0.0001909722222, limit: 28800, start: 28800, price: 550 }
                // members.update({ _id: req.params.id }, { $inc: { point: be1.price } })
            members.update({ _id: upline }, { $inc: { point: be1.price } })
            ticket.insert({
                memID: req.params.id,
                pkname: be1.name,
                package: 2,
                persec: be1.payment,
                limit: be1.limit,
                running: be1.start,
                dateBuy: dateS,
                dateExpire: adateSettign(60)
            })


        } else if (s === 6) {
            let be1 = { name: 'Standard Promotion', payment: 0.000462962963, limit: 28800, start: 28800, price: 1250 }
                // members.update({ _id: req.params.id }, { $inc: { point: be1.price } })
            members.update({ _id: upline }, { $inc: { point: be1.price } })
            ticket.insert({
                memID: req.params.id,
                pkname: be1.name,
                package: 3,
                persec: be1.payment,
                limit: be1.limit,
                running: be1.start,
                dateBuy: dateS,
                dateExpire: adateSettign(60)
            })

        } else if (s === 7) {
            let be1 = { name: 'Standard Promotion', payment: 0.000462962963, limit: 28800, start: 28800, price: 1250 }
            let be2 = { name: 'Standard Promotion', payment: 0.000462962963, limit: 28800, start: 28800, price: 1250 }
                // members.update({ _id: req.params.id }, { $inc: { point: be1.price * 2 } })
            members.update({ _id: upline }, { $inc: { point: be1.price * 2 } })
            ticket.insert({
                memID: req.params.id,
                pkname: be1.name,
                package: 3,
                persec: be1.payment,
                limit: be1.limit,
                running: be1.start,
                dateBuy: dateS,
                dateExpire: adateSettign(60)
            }).then(() => {
                ticket.insert({
                    memID: req.params.id,
                    pkname: be2.name,
                    package: 3,
                    persec: be2.payment,
                    limit: be2.limit,
                    running: be2.start,
                    dateBuy: dateS,
                    dateExpire: adateSettign(60)
                })
            })
        } else if (s === 8) {
            let be1 = { name: 'Silver Promotion', payment: 0.001397569444, limit: 28800, start: 28800, price: 3500 }
                // members.update({ _id: req.params.id }, { $inc: { point: be1.price } })
            members.update({ _id: upline }, { $inc: { point: be1.price } })
            ticket.insert({
                memID: req.params.id,
                pkname: be1.name,
                package: 4,
                persec: be1.payment,
                limit: be1.limit,
                running: be1.start,
                dateBuy: dateS,
                dateExpire: adateSettign(60)
            })
        }



    }



    function addTikedTouser(upline) {

        // members.update({ _id: req.params.id }, { $inc: { point: calculations(req.body.options).price } })

        ticket.insert({
            memID: req.params.id,
            pkname: calculations(req.body.options).name,
            package: req.body.options,
            persec: calculations(req.body.options).payment,
            limit: calculations(req.body.options).limit,
            running: calculations(req.body.options).start,
            dateBuy: dateS,
            dateExpire: adateSettign(60)
        }).then((doc) => {

            buyCartToKockpe(doc._id)
                // if (req.body.options >= 5) {
                //     promotions_freeonebuyone(req.body.options, upline)
                // }
        })
    }

    function uplineDeting(uid, a) {
        // members.update({ _id: uid }, { $inc: { point: a } })
        var recive = a * 5 / 100;
        members.update({ _id: uid }, {
            $inc: {
                wallet: recive
            },
            $push: {
                transection: {
                    amount: recive,
                    type: 'puse',
                    date: dateS,
                    detail: "ได้รับเงินจาก ค่าแนะนำ 5%"
                }
            }
        })
    }

    function updateData(a) {
        members.findOneAndUpdate({ _id: req.params.id }, { $inc: { cash: -a } }).then(doc => {
            io.emit("CASH_" + req.params.id, { amount: a, type: "dis" })

            addTikedTouser(doc.ref)
            if (doc.ref !== '') {
                uplineDeting(doc.ref, a)
            }
            io.emit("promotions", { fullname: doc.fullname, point: doc.point, buy: calculations(req.body.options).name })
            return res.status(200).json({
                status: 200,
                msg: 'สั่งซื้อสำเร็จแล้ว',
                date: dateS
            })
        })
    }

    function buyCartToKockpe(a) {
        members.findOne({ _id: req.params.id }).then(doc => {

            post.insert({
                memID: req.params.id,
                fullname: doc.fullname,
                post: doc.fullname + "ได้ทำการซื้อ Card " + calculations(req.body.options).name,
                buyCard: req.body.options,
                CardID: a.toString(),
                date: dateS,
                like: 0
            }).then(doc => {
                io.emit("feeds", {
                    memID: req.params.id,
                    fullname: doc.fullname,
                    post: doc.fullname + "ได้ทำการซื้อ Card " + calculations(req.body.options).name,
                    buyCard: req.body.options,
                    CardID: a.toString(),
                    date: dateS,
                    like: 0,
                    _id: doc._id
                })
            })
        })

    }

    if (req.params.id) {


        members.findOne({ _id: req.params.id }, { fields: { cash: 1 } }).then(doc => {
            if (doc.cash >= packed(req.body.options)) {
                updateData(packed(req.body.options))
                members.update({ _id: req.params.id }, {
                    $push: {
                        transection: {
                            amount: -Number(packed(req.body.options)),
                            type: 'down',
                            date: dateS,
                            detail: "ซื้อ Card " + calculations(req.body.options).name + " ใช้ CASH"
                        }
                    }
                })



                return res.status(200).json({
                    status: 200,
                    msg: 'โอนสำเร็จ',
                    date: dateS
                })
            } else {
                return res.status(200).json({
                    status: 404,
                    msg: 'ยอดเงินในกระเป๋า CASH ไม่เพียงพอ',
                    date: dateS
                })
            }

        })


    }



});


rounter.route('/members/:id/tikedOnlineStreaming').get((req, res) => {
    if (req.params.id) {
        ticket.find({ memID: req.params.id, dateExpire: { $gte: dateS } }, { fields: { _id: 1 } }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
});

rounter.route('/members/:id/evenLog').post((req, res) => {
    if (req.params.id) {
        ticket.findOne({ _id: req.body.card }, { fields: { log: 1 } }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc.log,
                date: dateS
            })
        })
    }
})

rounter.route('/members/:id/tikedOne/:ticket').get((req, res) => {
    if (req.params.id) {
        ticket.findOne({ _id: req.params.ticket }, { fields: { memID: 1, pkname: 1, package: 1, persec: 1, limit: 1, running: 1, dateBuy: 1, dateExpire: 1, endWorker: 1, startWorker: 1 } }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
});

rounter.route('/members/:id/WorkingArowsn/:ticket').get((req, res) => {
    if (req.params.id) {
        ticket.update({ _id: req.params.ticket }, {
            $set: {
                limit: 28800,
                running: 0,
                startWorker: dateS,
                endWorker: atimeSettign(8)
            },
            $push: {
                log: {
                    date: dateS,
                    status: "Run"
                }
            }
        }).then(() => {
            return res.status(200).json({
                status: 200,
                data: req.params.ticket,
                date: dateS
            })
        })
    }
})

rounter.route('/members/:id/xOxoErz/:ticket').post((req, res) => {
    function pkset(a) {

        if (a === 1) {
            return { name: 'Beginner', payment: 0.00009722222222, limit: 28800, start: 28800 }

        } else if (a === 2) {
            return { name: 'Member', payment: 0.0001909722222, limit: 28800, start: 28800 }

        } else if (a === 3) {
            return { name: 'Standard', payment: 0.000462962963, limit: 28800, start: 28800 }

        } else if (a === 4) {
            return { name: 'Silver', payment: 0.001397569444, limit: 28800, start: 28800 }

        } else if (a === 5) {
            return { name: 'Gold', payment: 0.002916666667, limit: 28800, start: 28800 }

        } else if (a === 6) {
            return { name: 'Platinum', payment: 0.006597222222, limit: 28800, start: 28800 }
        } else if (a === 7) {

            return { name: 'Exclusive', payment: 0.01354166667, limit: 28800, start: 28800 }
        } else if (a === 8) {
            return { name: 'Premium', payment: 0.0234375, limit: 28800, start: 28800 }
        }

    }

    function checkDateSet() {
        ticket.findOne({ _id: req.params.ticket }, { fields: { startWorker: 1, dateExpire: 1 } }).then(doc => {

            if (doc.dateExpire > dateS) {

                // console.log(datecheckingCliam(doc.startWorker))
                if (datecheckingCliam(doc.startWorker) >= 28800) {

                    members.update({ _id: req.params.id }, {
                        $inc: {
                            wallet: isbonus
                        }
                    }).then(() => {



                        return res.status(200).json({
                            status: 200,
                            data: "updated",
                            date: dateS
                        })
                    })

                    io.emit("WALLET_" + req.params.id, { amount: isbonus, type: "update" })
                    io.emit("claim", { userClaim: req.params.id, amount: isbonus, date: dateS })
                    updateTicked();
                    getRef();
                } else {
                    return res.status(200).json({
                        status: 200,
                        data: "error",
                        date: dateS
                    })
                }

            }



        })
    }

    function updateTicked() {
        ticket.update({ _id: req.params.ticket }, {
            $unset: { endWorker: 1, startWorker: 1 },
            $set: { running: 28800 },
            $push: {
                log: {
                    date: dateS,
                    status: "Claim"
                }
            }
        })
        members.update({ _id: req.params.id }, {
            $push: {
                transection: {
                    amount: isbonus,
                    type: 'puse',
                    date: dateS,
                    detail: "Claim Card สำเร็จ"
                }
            }
        })

    }

    function getRef() {
        members.findOne({ _id: req.params.id }, { fields: { ref: 1 } }).then(doc => {
            if (doc.ref) {
                members.update({ _id: doc.ref }, {
                    $inc: {
                        wallet: Upline
                    },
                    $push: {
                        transection: {
                            amount: Upline,
                            type: 'puse',
                            date: dateS,
                            detail: "ได้รับเงินจาก Commission 10%"
                        }
                    }
                })
            }
        })
    }


    if (req.params.id) {


        var isbonus = pkset(Number(req.body.pkg)).payment * 28800
        var Upline = isbonus * 10 / 100
        checkDateSet();


    }
})

rounter.route('/members/:id/history').get((req, res) => {
    if (req.params.id) {
        members.findOne({ _id: req.params.id }, { fields: { transection: 1 } }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
});

rounter.route('/members/:id/bankCheck').get((req, res) => {
    if (req.params.id) {
        members.findOne({ _id: req.params.id }).then(doc => {
            if (doc.bank === undefined) {
                return res.status(200).json({
                    status: 200,
                    data: "add_true",
                    date: dateS
                })
            } else {
                return res.status(200).json({
                    status: 404,
                    data: doc.bank[0],
                    date: dateS
                })
            }
        })
    }


});

rounter.route('/members/:id/addbank').post((req, res) => {
    function checkDataSame() {
        members.count({
            banknumber: req.body.banknumber
        }).then(doc => {
            if (doc >= 1) {
                return res.status(200).json({
                    status: 400,
                    msg: "ผิดพลาดมีการใช้งานบัญชีซ้ำ",
                    date: dateS
                })
            } else {
                members.update({ _id: req.params.id }, {
                    $push: {
                        bank: {
                            ...req.body
                        }
                    }
                }).then(() => {
                    return res.status(200).json({
                        status: 200,
                        data: "200 Status",
                        date: dateS
                    })
                })

            }
        })
    }

    if (req.params.id) {
        checkDataSame()
    }
})

rounter.route('/members/:id/withdarw').post((req, res) => {




    function withdraws() {
        members.update({ _id: req.params.id }, {
            $inc: {
                wallet: -Number(req.body.amount)
            },
            $set: {
                banknumber: req.body.banknumber
            }
        })

        withdraw.insert({
            memID: req.params.id,
            amount: Number(req.body.amount),
            bank: req.body.bank,
            bankaccount: req.body.bankaccount,
            banknumber: req.body.banknumber,
            phone: req.body.phone,
            date: dateS,
            type: 0,
            profile: req.body.profile
        }).then(() => {

            if (req.body.amount > 500) {
                post.insert({
                    memID: req.params.id,
                    fullname: req.body.profile.fullname,
                    post: "ยินดีด้วย " + req.body.profile.fullname + " ทำรายการถอนเงิน จำนวน " + req.body.amount + " บาท สำเร็จ!!",
                    type: "withdraws",
                    date: dateS,
                    like: 0
                }).then(doc => {

                    io.emit("feeds", {
                        memID: req.params.id,
                        fullname: req.body.profile.fullname,
                        post: "ยินดีด้วย " + req.body.profile.fullname + " ทำรายการถอนเงิน จำนวน " + req.body.amount + " บาท สำเร็จ!!",
                        type: "withdraws",
                        date: dateS,
                        like: 0,
                        _id: doc._id
                    })
                })

            }


            // const data = {
            //     from: 'CARDRUNNER <cardrunner20@gmail.com>',
            //     to: doc.profile.email,
            //     subject: 'ยืนยันคำสั่งถอนเงิน จากบัญชี ' + doc.profile.fullname,
            //     template: "veryfy",
            //     'v:fullname': doc.profile.fullname,
            //     'v:amount': req.body.amount,
            //     'v:id': 'http://cardrunner.co/confirm/token/' + doc._id

            // };

            // mg.messages().send(data, function(error, body) {
            //     console.log(body);
            // });
            // var mailOptions = {
            //     from: 'crnservice2020@gmail.com',
            //     to: doc.profile.email,
            //     subject: 'ยืนยันคำสั่งถอนเงิน จากบัญชี ' + doc.profile.fullname,
            //     html: '<h2>คำสั่งถอนใหม่ </h2>ถอนเงิน <u>จำนวน ' + req.body.amount + ' บาท</u> <br>ยืนยันคำสั่งถอน <a href=https://cardrunner.co/confirm/token/' + doc._id + '>ยืนยัน</a>',
            // };

            // transporter.sendMail(mailOptions);
        })
    }
    if (req.params.id) {
        members.findOne({ _id: req.params.id }, { fields: { wallet: 1 } }).then(doc => {
            if (doc.wallet >= 100) {
                if (req.body.amount > doc.wallet) {
                    return res.status(200).json({
                        status: 480,
                        msg: "Error Code 404 (#Withdraw failer)",
                        date: dateS
                    })
                } else {
                    io.emit("WALLET_" + req.params.id, { amount: req.body.amount, type: "dis" })
                    withdraws()
                    return res.status(200).json({
                        status: 200,
                        data: "succ",
                        date: dateS
                    })
                }
            } else {
                return res.status(200).json({
                    status: 480,
                    msg: "Error Code 404 (#Withdraw failer)",
                    date: dateS
                })
            }


        })
    }
})

rounter.route('/members/:id/checkWithdrawPerday').get((req, res) => {


    if (req.params.id) {
        var midnightUTCDate = new Date().getUTCFullYear() + "-" + (new Date().getUTCMonth() + 1) + "-" + new Date().getUTCDate()
        withdraw.find({ memID: req.params.id, "date": { "$gte": new Date(midnightUTCDate) } }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
});

rounter.route('/members/:id/showwithdarw').get((req, res) => {
    if (req.params.id) {
        withdraw.find({ memID: req.params.id }, { sort: { date: -1 }, limit: 5 }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
});

rounter.route('/members/:id/onupload').post((req, res) => {
    if (req.params.id) {
        deposit.insert({
            memID: req.params.id,
            amount: Number(req.body.amount),
            dateTransfer: req.body.dateTransfer,
            time: req.body.time,
            picture: req.body.picture,
            CreatDate: dateS,
            status: 0,
            profile: req.body.profile
        }).then(() => {
            return res.status(200).json({
                status: 200,
                msg: "success",
                date: dateS
            })
        })
    }
})

rounter.route('/members/:id/loaddeposit').get((req, res) => {
    if (req.params.id) {
        deposit.find({ memID: req.params.id }, { sort: { CreatDate: -1 }, limit: 5 }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
})

rounter.route('/members/:id/affaliat').get((req, res) => {
    if (req.params.id) {
        members.find({ ref: req.params.id }, { fields: { email: 1, fullname: 1, Createis: 1 } }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
});

rounter.route('/members/:id/loadWallets').get((req, res) => {
    if (req.params.id) {
        members.findOne({ _id: req.params.id }, { fields: { wallet: 1, point: 1, cash: 1 } }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
})
rounter.route('/members/:id/loadChalenge').get((req, res) => {
    if (req.params.id) {
        members.find({ point: { $gte: 1000 } }, { fields: { point: 1, fullname: 1 }, sort: { point: -1 }, limit: 50 }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
});

rounter.route('/members/:id/transtowallet').post((req, res) => {
    function useaccess() {
        var totlas = (req.body.amount * 15 / 100) + req.body.amount
        members.findOneAndUpdate({ _id: req.params.id }, {
            $inc: {
                wallet: -Number(req.body.amount),
                cash: Number(totlas)
            },
            $push: {
                transfer: {
                    amount: req.body.amount,
                    date: dateS
                },
                transection: {
                    amount: -Number(req.body.amount),
                    type: 'down',
                    date: dateS,
                    detail: "โอนไปยัง CASH ได้รับเพิ่มอีก 15% " + req.body.amount * 15 / 100 + " รวม " + Number(totlas) + " CASH"
                }
            }
        }).then(() => {

            io.emit("WALLET_" + req.params.id, { amount: req.body.amount, type: "dis" })
            io.emit("CASH_" + req.params.id, { amount: totlas, type: "update" })
            return res.status(200).json({
                status: 200,
                data: "msg",
                date: dateS
            })
        })
    }
    if (req.params.id) {
        members.findOne({ _id: req.params.id }, { fields: { wallet: 1 } }).then(doc => {
            if (Number(req.body.amount) < doc.wallet) {
                useaccess()
            } else {
                return res.status(200).json({
                    status: 444,
                    data: "msg",
                    date: dateS
                })
            }
        })
    }
})
rounter.route('/members/:id/Loadtranstowallet').get((req, res) => {
    if (req.params.id) {
        members.find({ _id: req.params.id }, { fields: { transfer: 1 } }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
})

///////loginAdmin

rounter.route('/admin/login').post((req, res) => {
    if (req.body.email === "jonhzenol" && req.body.pwd === "!bluebell147") {
        return res.status(200).json({
            status: 200,
            msg: "pass",
            codeGEN: '1411852110412345xc48sd6aw1d20awd14748zc-',
            date: dateS
        })
    } else {
        return res.status(200).json({
            status: 123,
            msg: "error",
            date: dateS
        })
    }
})

rounter.route('/admin/:token/loadallwallet').get((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        members.aggregate([{
            $group: {
                _id: "",
                wallet: { $sum: "$wallet" }
            }
        }]).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
})

rounter.route('/admin/:token/findMembers').post((req, res) => {

    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {

        if (req.body.type === "email") {
            members.find({ email: { $regex: req.body.search } }, { fields: { fullname: 1, email: 1, pwd: 1, wallet: 1 }, sort: { Createis: -1 } }).then(doc => {
                return res.status(200).json({
                    status: 200,
                    data: doc,
                    date: dateS
                })
            })
        }

        if (req.body.type === "password") {
            members.find({ pwd: req.body.search }, { fields: { fullname: 1, email: 1, pwd: 1, wallet: 1 }, sort: { Createis: -1 } }).then(doc => {
                return res.status(200).json({
                    status: 200,
                    data: doc,
                    date: dateS
                })
            })
        }

        if (req.body.type === "bankaccount") {
            members.find({ banknumber: req.body.search }, { fields: { fullname: 1, email: 1, pwd: 1, wallet: 1 }, sort: { Createis: -1 } }).then(doc => {
                return res.status(200).json({
                    status: 200,
                    data: doc,
                    date: dateS
                })
            })
        }


        if (req.body.type === "fullname") {
            members.find({ fullname: { $regex: req.body.search } }, { fields: { fullname: 1, email: 1, pwd: 1, wallet: 1 }, sort: { Createis: -1 } }).then(doc => {
                return res.status(200).json({
                    status: 200,
                    data: doc,
                    date: dateS
                })
            })
        }

        if (req.body.type === "id") {
            members.find({ _id: req.body.search }, { fields: { fullname: 1, email: 1, pwd: 1, wallet: 1 }, sort: { Createis: -1 } }).then(doc => {
                return res.status(200).json({
                    status: 200,
                    data: doc,
                    date: dateS
                })
            })
        }




    }
})

rounter.route('/admin/:token/loadmembers').get((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        members.find({}, { fields: { fullname: 1, email: 1, pwd: 1, wallet: 1 }, sort: { Createis: -1 } }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
})


rounter.route('/admin/:token/deposit_approve').get((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        deposit.find({ status: 1 }, { sort: { CreatDate: -1 } }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }

})


rounter.route('/admin/:token/deposit').get((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        deposit.find({ status: 0 }, { sort: { CreatDate: 1 } }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }

})

rounter.route('/admin/:token/deposit/:id/error').get((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        deposit.update({ _id: req.params.id }, {
            $set: { status: 2 }

        }).then(() => {
            return res.status(200).json({
                status: 200,
                msg: "000",
                date: dateS
            })
        })
    }
})



rounter.route('/admin/:token/deposit/add').post((req, res) => {
    function sendtMall() {
        members.findOne({ _id: req.body.memID }, { fields: { fullname: 1, email: 1 } }).then(doc => {

            const data = {
                from: 'CARDRUNNER <cardrunner20@gmail.com>',
                to: doc.email,
                subject: 'ฝากเงินสำเร็จ ' + doc.fullname,
                template: "deposit",
                'v:fullname': doc.fullname,
                'v:amount': req.body.amount,
            };

            mg.messages().send(data, function(error, body) {
                console.log(body);
            });

        })


    }

    function updateMemberAndTransection() {
        members.update({ _id: req.body.memID }, {
            $inc: {
                cash: Number(req.body.amount)
            },
            $push: {
                transection: {
                    amount: Number(req.body.amount),
                    type: 'puse',
                    date: dateS,
                    detail: "ฝากเงินเข้าสู่ระบบสำเร็จ ในกระเป๋า CASH"
                }
            }
        }).then(() => {
            sendtMall()
            io.emit("CASH_" + req.body.memID, { amount: req.body.amount, type: "update" })
            return res.status(200).json({
                status: 200,
                msg: "000",
                date: dateS
            })
        })
    }
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        deposit.update({ _id: req.body.id }, {
            $set: { status: 1 }
        }).then(() => {
            updateMemberAndTransection()

        })
    }

})

rounter.route('/admin/:token/withdarw').get((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        withdraw.find({ type: 0 }, { sort: { date: 1 } }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        });
    }

})

rounter.route('/admin/:token/withdarwDangerlas').get((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        withdraw.find({ type: 4 }, { sort: { date: 1 } }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        });
    }

})
rounter.route('/admin/:token/withdarw/:id/error').get((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        withdraw.update({ _id: req.params.id }, {
            $set: {
                type: 2
            }
        }).then(() => {
            return res.status(200).json({
                status: 200,
                msg: "cancel",
                date: dateS
            })
        })
    }
})

rounter.route('/admin/:token/withdarw/accept').post((req, res) => {
    function allwithdraws() {
        allwithdraw.insert({
            all: Number(req.body.amount)
        })
    }

    function updateState() {
        members.update({ _id: req.body.memID }, {
            $push: {
                transection: {
                    amount: -Number(req.body.amount),
                    type: 'dis',
                    date: dateS,
                    detail: "ถอนเงินสำเร็จแล้ว"
                }
            }
        }).then(() => {
            allwithdraws()
            return res.status(200).json({
                status: 200,
                msg: "passing",
                date: dateS
            })
        })
    }

    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        withdraw.update({ _id: req.body.id }, {
            $set: {
                type: 1
            }
        }).then(() => {
            updateState()
        })
    }

})

rounter.route('/admin/:token/witdrawBlock').post((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {

        withdraw.update({ _id: req.body.id }, { $set: { type: 4, commnet: req.body.comment } }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: "ok",
                date: dateS
            })
        })


    }
})

rounter.route('/admin/:token/cards').get((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {

        ticket.aggregate([{
            $group: {
                _id: "$pkname",
                pkname: { $sum: 1 }
            }
        }]).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })

    }
});

rounter.route('/admin/:token/edit/:id').get((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        members.findOne({ _id: req.params.id }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        });
    }
});

rounter.route('/admin/:token/edit/:id/update').post((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        members.update({
            _id: req.params.id
        }, {
            $set: {
                pwd: req.body.pwd,
                bank: [{
                    bankaccount: req.body.bankaccount,
                    bank: req.body.bank,
                    banknumber: req.body.banknumber,
                    phone: req.body.phone,
                }]

            }
        }).then(() => {
            return res.status(200).json({
                status: 200,
                msg: "pass",
                date: dateS
            })
        })
    }
})

rounter.route('/admin/:token/ipflipers').get((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        serverIP.aggregate([{
            $group: {
                _id: "$ip",
                ip: { $sum: 1 }
            }
        }]).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
})



rounter.route('/admin/:token/emailIP').get((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        serverIP.find({}).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
})


rounter.route('/admin/:token/findIP').post((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {

        if (req.body.type === "email") {
            serverIP.find({ email: { $regex: req.body.search } }).then(doc => {
                return res.status(200).json({
                    status: 200,
                    data: doc,
                    date: dateS
                })
            })
        }

        if (req.body.type === "ip") {
            serverIP.find({ ip: { $regex: req.body.search } }).then(doc => {
                return res.status(200).json({
                    status: 200,
                    data: doc,
                    date: dateS
                })
            })
        }
    }
})

rounter.route('/admin/:token/ip/:ip').get((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        serverIP.find({ ip: req.params.ip }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
})

rounter.route('/admin/:token/ip/cancle').post((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        serverIP.remove({ ip: req.body.ip }).then(() => {
            return res.status(200).json({
                status: 200,
                msg: "confirm",
                date: dateS
            })
        });
    }
});

rounter.route('/admin/:token/loadUpline/:id').get((req, res) => {
    function searchUpline(up) {
        members.findOne({ _id: up }, { fields: { fullname: 1, email: 1, pwd: 1, wallet: 1 } }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }


    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {


        members.findOne({ _id: req.params.id }, { fields: { ref: 1 } }).then(doc => {
            if (doc) {
                searchUpline(doc.ref)
            }
        })



    }
});

rounter.route('/admin/:token/withdrawSetday').get((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        withdraw.aggregate([
            { $match: { type: 1 } },
            {
                $group: {
                    _id: { $dateToString: { format: "%Y-%m-%d", date: "$date" } },
                    amount: { $sum: "$amount" },
                    lister: { $sum: 1 }
                }
            }
        ]).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
});


rounter.route('/admin/:token/deptSetday').get((req, res) => {
    if (req.params.token === '1411852110412345xc48sd6aw1d20awd14748zc-') {
        deposit.aggregate([
            { $match: { status: 1 } },
            {
                $group: {
                    _id: { $dateToString: { format: "%Y-%m-%d", date: "$CreatDate" } },
                    amount: { $sum: "$amount" },
                    lister: { $sum: 1 }
                }
            }

        ]).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
});

rounter.route('/checkConnecting').get((req, res) => {
    return res.status(200).json({
        status: 200,
        mgs: "Connecting OK",
        date: dateS
    })
});

rounter.route('/confirmdata/token').post((req, res) => {
    function updateState() {
        withdraw.update({ _id: req.body.token }, { $set: { type: 0 } }).then(() => {
            return res.status(200).json({
                status: 200,
                mgs: "Connecting OK",
                date: dateS
            })
        })
    }
    withdraw.findOne({ _id: req.body.token }).then(doc => {
        if (doc) {
            if (doc.type === 3) {
                updateState()
            } else {
                return res.status(200).json({
                    status: 404,
                    mgs: "Token หมดอายุุ",
                    date: dateS
                })
            }
        }
    })
});



//socail path

rounter.route('/community/event/:token/mypost').get((req, res) => {
    if (req.params.token) {
        post.find({ memID: req.params.token }, { sort: { _id: -1 }, limit: 50 }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
})

rounter.route('/community/event/:token/firepost').get((req, res) => {
    if (req.params.token) {
        post.find({}, { sort: { c_com: -1 }, limit: 30 }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
})


rounter.route('/community/event/:token/loadsomting').get((req, res) => {
    if (req.params.token) {
        post.find({}, { sort: { _id: -1 }, limit: 30 }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
})

rounter.route('/community/event/:token/loadOnce/:postid').get((req, res) => {
    if (req.params.token) {
        post.findOne({ _id: req.params.postid }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
})

rounter.route('/community/event/:token/post').put((req, res) => {

    if (req.params.token) {


        post.insert({
            memID: req.params.token,
            fullname: req.body.fullname,
            post: req.body.post,
            date: dateS,
            like: 0
        }).then((doc) => {

            io.emit("feeds", {
                _id: doc._id,
                memID: req.params.token,
                fullname: req.body.fullname,
                post: req.body.post,
                date: dateS,
                like: 0
            })
            return res.status(200).json({
                status: 200,
                mgs: "โพสสำเร็จแล้ว",
                date: dateS
            })
        })


    }
})




rounter.route('/community/event/:token/comment').put((req, res) => {
    if (req.params.token) {

        post.findOneAndUpdate({ _id: req.body.url }, {
            $inc: {
                c_com: 1
            },
            $push: {
                comment: {
                    date: dateS,
                    fullname: req.body.fullname,
                    comment: req.body.commnet
                }
            }
        }).then((doc) => {

            if (req.params.token != doc.memID) {
                members.update({ _id: doc.memID }, {
                    $inc: {
                        noti: 1
                    },
                    $push: {
                        notification: {
                            title: req.body.fullname + "ได้แสดงความเห็นโพสของคุณ",
                            date: dateS,
                            url: req.body.url,
                            active: 'yes'
                        }
                    }
                }).then(() => {
                    io.emit("noti_" + doc.memID, {
                        update: "up"
                    })
                })
            }
            io.emit("comment_" + req.body.url, {
                commentID: req.body.url,
                fullname: req.body.fullname,
                comment: req.body.commnet,
                date: dateS
            })
            return res.status(200).json({
                status: 200,
                mgs: "โพสสำเร็จแล้ว",
                date: dateS
            })
        })
    }
})


rounter.route('/community/event/:token/like').put((req, res) => {
    if (req.params.token) {
        post.update({ _id: req.body.id }, {
            $inc: {
                like: 1
            }
        }).then(() => {
            return res.status(200).json({
                status: 200,
                loaddaed: "usefull",
                date: dateS
            })
        })
    }

});


rounter.route('/community/event/:token/loadNoti').get((req, res) => {
    if (req.params.token) {
        members.findOne({ _id: req.params.token }, { fields: { noti: 1 } }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
})

rounter.route('/community/event/:token/removeNotifications').get((req, res) => {
    if (req.params.token) {
        members.update({ _id: req.params.token }, { $unset: { notification: 1 } }).then(() => {
            return res.status(200).json({
                status: 200,
                data: 'aa',
                date: dateS
            })
        })
    }
})


rounter.route('/community/event/:token/loadListNotifications').get((req, res) => {
    if (req.params.token) {
        members.findOne({ _id: req.params.token }, { fields: { notification: 1 } }).then(doc => {
            return res.status(200).json({
                status: 200,
                data: doc,
                date: dateS
            })
        })
    }
})

rounter.route('/community/event/:token/crearNotifications').get((req, res) => {
    if (req.params.token) {
        members.update({ _id: req.params.token }, {
            $set: {
                noti: 0
            }
        }).then(() => {
            io.emit("noti_" + req.params.token, {
                update: "down"
            })
            return res.status(200).json({
                status: 200,
                data: "ok",
                date: dateS
            })

        })
    }
})


module.exports = rounter;



// db.collection.aggregate({
//     $group: {
//         _id: '',
//         subida: { $sum: '$subida' }
//     }
//  }
// )

// [ {
//     $group: {
//       _id:"$pkname",
//       pkname: { $sum: 1}
//     }
//   } ]