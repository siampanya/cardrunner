import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import LoadScript from 'vue-plugin-load-script';
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueSession from 'vue-session'
import VueSocketIO from 'vue-socket.io'
import Vue2Filters from 'vue2-filters'
import VueClipboards from 'vue-clipboards';
import VueNumber from 'vue-number-animation'
import VuePromptpayQr from 'vue-promptpay-qr'
import Notifications from 'vue-notification'



Vue.use(VueNumber)
Vue.use(Vue2Filters)
Vue.use(require('vue-moment'));
Vue.use(VueClipboards);

Vue.use(new VueSocketIO({
    debug: false,
    connection: 'https://cardrunner.co'
        //server connection: 'https://cardrunner.co'
}))


Vue.use(Notifications)
Vue.use(VuePromptpayQr)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueSession)


Vue.config.productionTip = false
Vue.use(VueAxios, axios)
Vue.use(LoadScript);

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.loadScript("https://kit.fontawesome.com/258d21a156.js")



Vue.loadScript("https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit")

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')