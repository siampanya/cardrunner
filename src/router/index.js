import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/about',
        name: 'About',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/About.vue')
    },
    {
        path: '/challenge',
        name: 'challenge',
        component: () =>
            import ('../views/challenge.vue')
    },

    {
        path: '/register',
        name: 'registre',
        component: () =>
            import ('../views/register.vue')
    },

    {
        path: '/confirm/token/:id',
        name: 'confirmmall',
        component: () =>
            import ('../views/confirmmall.vue')
    },
    {
        path: '/login',
        name: 'login',
        component: () =>
            import ('../views/login.vue')
    },
    {
        path: '/panner',
        name: 'panner',
        component: () =>
            import ('../views/panner.vue')
    },
    {
        path: '/cp',
        name: 'contrils',
        component: () =>
            import ('../views/member/auth.vue'),
        children: [{
                path: '/cp',
                name: 'controlpan',
                component: () =>
                    import ('../views/member/dashbord.vue')
            },
            {
                path: '/cp/ticket',
                name: 'tiked',
                component: () =>
                    import ('../views/member/tikets.vue')
            },
            {
                path: '/cp/history',
                name: 'tiked',
                component: () =>
                    import ('../views/member/transections.vue')
            },
            {
                path: '/cp/withdraw',
                name: 'withdraw',
                component: () =>
                    import ('../views/member/withdraw.vue')
            },
            {
                path: '/cp/deposit',
                name: 'deposit',
                component: () =>
                    import ('../views/member/deposit.vue')
            },
            {
                path: '/cp/affaliate',
                name: 'deposit',
                component: () =>
                    import ('../views/member/affaliate.vue')
            },
            {
                path: '/cp/challenge',
                name: 'challenge',
                component: () =>
                    import ('../views/member/challenge.vue')
            },
            {
                path: '/cp/transfertocash',
                name: 'transfertocash',
                component: () =>
                    import ('../views/member/transfertocash.vue')
            },

            {
                path: '/cp/community',
                name: 'community',
                component: () =>
                    import ('../views/member/community.vue'),
                children: [{
                        path: '/cp/community/feed',
                        name: 'feed',
                        component: () =>
                            import ('../views/member/socials/feed.vue')
                    },
                    {
                        path: '/cp/community/yourpost',
                        name: 'yourpost',
                        component: () =>
                            import ('../views/member/socials/yourpost.vue')
                    },

                    {
                        path: '/cp/community/feed/:id',
                        name: 'comment',
                        component: () =>
                            import ('../views/member/socials/comment.vue')
                    },
                    {
                        path: '/cp/community/alert',
                        name: 'alert',
                        component: () =>
                            import ('../views/member/socials/alert.vue')
                    },
                    {
                        path: '/cp/community/fire',
                        name: 'alert',
                        component: () =>
                            import ('../views/member/socials/fire.vue')
                    }
                ]
            },
            {
                path: '/cp/logout',
                name: 'logout',
                component: () =>
                    import ('../views/member/logout.vue')
            },
        ]
    },
    {
        path: '/mett',
        name: 'mett',
        component: () =>
            import ('../views/adlogin.vue')
    },
    {
        path: '/zibra',
        name: 'roooting',
        component: () =>
            import ('../views/addroot/auth.vue'),
        children: [{
                path: '/zibra',
                name: 'adjustdata',
                component: () =>
                    import ('../views/addroot/allsetting.vue')
            },
            {
                path: '/zibra/addmoney',
                name: 'addmoney',
                component: () =>
                    import ('../views/addroot/addmoney.vue')
            },
            {
                path: '/zibra/withdraw',
                name: 'withdraw',
                component: () =>
                    import ('../views/addroot/withdraw.vue')
            },
            {
                path: '/zibra/edit/:id',
                name: 'editmem',
                component: () =>
                    import ('../views/addroot/editmem.vue')
            },
            {
                path: '/zibra/ip',
                name: 'ip',
                component: () =>
                    import ('../views/addroot/ip.vue')
            },
            {
                path: '/zibra/ipemail',
                name: 'ipemail',
                component: () =>
                    import ('../views/addroot/ipemail.vue')
            },
            {
                path: '/zibra/ip/:id',
                name: 'ipdetails',
                component: () =>
                    import ('../views/addroot/ipdetails.vue')
            },

            {
                path: '/zibra/withdraw_perdays',
                name: 'withdraw_perdays',
                component: () =>
                    import ('../views/addroot/withdraw_perdays.vue')
            },

            {
                path: '/zibra/addmoney_perdays',
                name: 'addmoney_perdays',
                component: () =>
                    import ('../views/addroot/addmoney_perday.vue')
            },
            {
                path: '/zibra/approve_depo',
                name: 'approve_depo',
                component: () =>
                    import ('../views/addroot/approve_depo.vue')
            },
            {
                path: '/zibra/withdrawDanger',
                name: 'withdraw_perdays',
                component: () =>
                    import ('../views/addroot/withdrawDanger.vue')
            }



        ]
    },

    {
        path: '*',
        name: 'error',
        component: () =>
            import ('../views/Error.vue')

    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router