const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
var cors = require('cors')
const app = express();

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cors());





app.use(express.static(path.join(__dirname, "dist")));
app.use('/api', require('./server/api.js'));


app.use('*', (req, res) => {

    res.sendFile(path.join(__dirname, "dist/index.html"));
});


const database = app.listen(4848, () => {
    const port = database.address().port;
    console.log("Server is using : " + port);
});


// const server = app.listen(80, () => {
//     const port = server.address().port;
//     console.log("Server is using : " + port);
// });
//